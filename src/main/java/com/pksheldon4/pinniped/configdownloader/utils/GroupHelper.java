package com.pksheldon4.pinniped.configdownloader.utils;

import com.pksheldon4.pinniped.configdownloader.model.Group;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class GroupHelper {
    public static final String TMP_CONFIG_DIR = "/tmp/config/";
    public static final File CONFIG_DIR = new File(TMP_CONFIG_DIR);

    public static Collection<String> getOidcGroups(OAuth2User principal) {
        return principal.getAttribute("groups");
    }

    public static List<Group> getAvailableGroups(OAuth2User principal) {
        Collection<String> oidcGroups = getOidcGroups(principal);
        if (oidcGroups.isEmpty()) {
            return Collections.emptyList();
        }
        List<File> availableGroups = new ArrayList<>();
        for (String group : oidcGroups) {
            File groupFile = new File(CONFIG_DIR + "/" + group);
            if (groupFile.isDirectory()) {
                availableGroups.add(groupFile);
            }
        }
        if (availableGroups.isEmpty()) {
            return Collections.emptyList();
        }
        List<Group> results = new ArrayList<>();
        for (File groupDirectory : availableGroups) {
            String[] filenames = groupDirectory.list((dir, name) -> {
                return name.toLowerCase().endsWith(".kubeconfig");
            });
            if (filenames != null && filenames.length > 0) {
                String groupName = groupDirectory.getAbsolutePath().substring(CONFIG_DIR.getAbsolutePath().length()+1);
                Group group = Group.of(groupName, Arrays.stream(filenames).collect(Collectors.toList()));
                results.add(group);
            }
        }
        return results;
    }
}
