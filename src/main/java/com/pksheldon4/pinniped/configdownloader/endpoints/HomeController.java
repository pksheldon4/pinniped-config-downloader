package com.pksheldon4.pinniped.configdownloader.endpoints;

import com.pksheldon4.pinniped.configdownloader.utils.GroupHelper;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
    @RequestMapping(value = "/")
    public String index(Model model, @AuthenticationPrincipal OAuth2User principal) {
        model.addAttribute("groups",  GroupHelper.getAvailableGroups(principal));
        return "download";
    }
}
