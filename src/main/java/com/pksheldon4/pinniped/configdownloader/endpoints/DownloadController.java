package com.pksheldon4.pinniped.configdownloader.endpoints;

import com.pksheldon4.pinniped.configdownloader.exceptions.InvalidDownloadAttemptException;
import com.pksheldon4.pinniped.configdownloader.model.Group;
import com.pksheldon4.pinniped.configdownloader.utils.GroupHelper;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.List;

import static com.pksheldon4.pinniped.configdownloader.utils.GroupHelper.TMP_CONFIG_DIR;

@RestController
public class DownloadController {


    @RequestMapping(value = "/downloadfile/{group}/{file}", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadFile(@PathVariable("group") String groupName, @PathVariable("file") String fileName, @AuthenticationPrincipal OAuth2User principal) throws Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s", fileName));

        //Verify that the group/filename requested is valid and the user haas access
        List<Group> availableGroups = GroupHelper.getAvailableGroups(principal);
        if (availableGroups.isEmpty()) {
            throw new InvalidDownloadAttemptException();
        }
        Group currentGroup = availableGroups.stream().filter(group -> group.getName().equals(groupName)).findFirst().orElseThrow(InvalidDownloadAttemptException::new);
        String outputFilename = String.format("%s/%s/%s", TMP_CONFIG_DIR, currentGroup.getName(), fileName);
        File outputFile = new File(outputFilename);
        if (outputFile.exists()) {
            FileSystemResource fileSystemResource = new FileSystemResource(outputFile);
            return ResponseEntity.ok()
                .headers(headers)
                .contentLength(fileSystemResource.contentLength())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(fileSystemResource);
        } else {
            throw new InvalidDownloadAttemptException();
        }
    }


}
