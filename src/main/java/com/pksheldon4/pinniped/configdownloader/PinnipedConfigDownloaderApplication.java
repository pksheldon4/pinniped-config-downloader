package com.pksheldon4.pinniped.configdownloader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PinnipedConfigDownloaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(PinnipedConfigDownloaderApplication.class, args);
    }

}
