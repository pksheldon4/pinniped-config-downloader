package com.pksheldon4.pinniped.configdownloader.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.FORBIDDEN)
public class InvalidDownloadAttemptException extends RuntimeException {
    private static final String message = "Attempting to download a file that either does not exist or you do not have access to.";
    public InvalidDownloadAttemptException() {
        super(message);
    }
}
