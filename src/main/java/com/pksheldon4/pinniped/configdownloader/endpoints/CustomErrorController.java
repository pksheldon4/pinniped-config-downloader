package com.pksheldon4.pinniped.configdownloader.endpoints;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("${server.error.path:${error.path:/error}}")
public class CustomErrorController implements ErrorController {

    @RequestMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());
            if (statusCode == HttpStatus.FORBIDDEN.value()) {
                return String.valueOf(statusCode);
            } else if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return String.valueOf(statusCode);
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return String.valueOf(statusCode);
            }
        }
        return "error";
    }
}
