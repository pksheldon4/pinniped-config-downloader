package com.pksheldon4.pinniped.configdownloader.model;

import lombok.Data;

import java.util.List;

@Data(staticConstructor="of")
public class Group {

    private final String name;
    private final List<String> filenames;

}
